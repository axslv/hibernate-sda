package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Department;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class DepartmentRepository {

    public static Department findById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Department p = session.find(Department.class, id);
        session.close();
        return p;
    }

    public static Department save(Department Department) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(Department);
        transaction.commit();
        session.close();
        return Department;
    }

    public static Department update(Department Department) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(Department);
        transaction.commit();
        session.close();
        return Department;
    }

    public static void delete(Department Department) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(Department);
        transaction.commit();
        session.close();
    }

}
