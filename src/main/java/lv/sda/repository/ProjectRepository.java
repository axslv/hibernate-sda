package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Project;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class ProjectRepository {

    public static Project findById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Project p = session.find(Project.class, id);
        session.close();
        return p;
    }

    public static Project save(Project project) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(project);
        transaction.commit();
        session.close();
        return project;
    }

    public static Project update(Project project) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(project);
        transaction.commit();
        session.close();
        return project;
    }

    public static void delete(Project project) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(project);
        transaction.commit();
        session.close();
    }

}
