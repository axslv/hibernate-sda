package lv.sda.repository;

import lv.sda.HibernateUtils;
import lv.sda.entity.Department;
import lv.sda.entity.Employee;
import lv.sda.entity.Employee;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class EmployeeRepository {

    public List<Employee> fetchAllEmployees() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        String selectAllDeptsHQL = "from Employee";
        Query query = session.createQuery(selectAllDeptsHQL);
        List<Employee> list = query.list();
        return list;
    }

    public List<Employee> fetchAllEmployeesCriteria() {
        Session session = HibernateUtils.getSessionFactory().openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
        CriteriaQuery<Employee> cr = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> from = cr.from(Employee.class);
        CriteriaQuery<Employee> select = cr.select(from);
        Query<Employee> query = session.createQuery(cr);
        return query.getResultList();
    }

    public static Employee findById(Integer id) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Employee p = session.find(Employee.class, id);
        session.close();
        return p;
    }

    public static Employee save(Employee employee) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.save(employee);
        transaction.commit();
        session.close();
        return employee;
    }

    public static Employee update(Employee employee) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.update(employee);
        transaction.commit();
        session.close();
        return employee;
    }

    public static void delete(Employee Employee) {
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(Employee);
        transaction.commit();
        session.close();
    }

}
