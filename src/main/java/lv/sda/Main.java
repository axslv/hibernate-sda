package lv.sda;

import lv.sda.entity.Project;
import lv.sda.repository.ProjectRepository;

public class Main {
    public static void main(String[] args) {
        //Select project by id
        Project project = ProjectRepository.findById(2);
        System.out.println(project.getDescription()); // Java - Fitness Web App
        //save new project
        Project p = new Project();
        p.setDescription("Hibernate at work");
        ProjectRepository.save(p);
        System.out.println(p.getProjectId());
        //update existing project
        p.setDescription("Hibernate at work updated");
        System.out.println(p.getDescription());
        ProjectRepository.update(p);
        //delete
        ProjectRepository.delete(p);

        //Create repositories for Employee and Department (EmployeeRepository and DepartmentRepository)

        //-------FOR EMPLOYEE--------
        //Demonstrate how EmployeeRepository works here (see example on lines 8-20)



        //-----FOR DEPARTMENTS--------
        //Demonstrate how DepartmentRepository works here (see example on lines 8-20)

        //ADDITIONAL
        //Create AbstractRepository and use it properly for any entity (be it Department or Employee or whatever

    }
}
