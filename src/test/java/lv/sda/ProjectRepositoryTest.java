package lv.sda;

import lv.sda.entity.Project;
import lv.sda.repository.ProjectRepository;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class ProjectRepositoryTest {

    @Test
    public void testFindById() {
        Project project = ProjectRepository.findById(2);
        assertThat(project.getDescription(), is("Java - Fitness Web App"));
    }

    @Test
    public void testProjectSaveAndUpdate() {
        Project p = new Project();
        p.setDescription("Hibernate at work");
        ProjectRepository.save(p);
        assertNotNull(p.getProjectId());
        p.setDescription("Hibernate at work updated");
        ProjectRepository.update(p);
        Project project = ProjectRepository.findById(p.getProjectId());
        assertThat(project.getDescription(), is("Hibernate at work updated"));
    }

    @Test
    public void testProjectDelete() {
        Project p = new Project();
        p.setDescription("Hibernate at work");
        ProjectRepository.save(p);
        assertNotNull(p.getProjectId());
        ProjectRepository.delete(p);
        Project project = ProjectRepository.findById(p.getProjectId());
        assertNull(project);
    }

}
